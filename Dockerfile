FROM golang:1.17 as builder

ENV CGO_ENABLED 0
ENV GOOS  linux

RUN go get -tags flatfile -u gitlab.com/opennota/widdly && \
  go install github.com/tomwright/dasel/cmd/dasel@master

FROM node:current-alpine3.13 as node-builder

COPY --from=builder /go/bin/dasel /usr/bin/

# dasel put string -f builder/tiddlywiki.info -p json '.plugins.[]' \

WORKDIR /opt/wiki
RUN npm install -g tiddlywiki@5.2.0 && \
  tiddlywiki builder --init && \
  for i in \
  "tiddlywiki/tiddlyweb" "tiddlywiki/filesystem" "tiddlywiki/highlight" "tiddlywiki/codemirror" "tiddlywiki/codemirror-closebrackets" "tiddlywiki/codemirror-closetag" "tiddlywiki/codemirror-fullscreen-editing" "tiddlywiki/codemirror-keymap-vim" "tiddlywiki/codemirror-mode-css" "tiddlywiki/codemirror-mode-htmlembedded" "tiddlywiki/codemirror-mode-htmlmixed" "tiddlywiki/codemirror-mode-javascript" "tiddlywiki/codemirror-mode-markdown" "tiddlywiki/codemirror-mode-x-tiddlywiki" "tiddlywiki/codemirror-mode-xml" "tiddlywiki/codemirror-search-replace"; \
  do dasel put string \
  -f builder/tiddlywiki.info \
  -p json '.plugins.[]' \
  $i \
  ; done && \
  tiddlywiki builder --build index


FROM alpine:latest

COPY --from=builder /go/bin/widdly /usr/bin/widdly

VOLUME /vol/data

WORKDIR /opt/wiki

COPY --from=node-builder /opt/wiki/builder/output/index.html /opt/wiki/

RUN chmod 755 /usr/bin/widdly

CMD /usr/bin/widdly -http :1338 -db /vol/data
